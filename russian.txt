! #CV-244
zen.yandex.ru##div[class*="is-parent-size_undefined"]
~market.yandex.ru,~market.yandex.kz,~market.yandex.by,~market.yandex.ua,~telephony.yandex.ru,yandex.ru,yandex.kz,yandex.uz,yandex.by,yandex.ua,yandex.fr##div[class*="item_type_"][class*="item_rendered"]
||razlozhi.ru^$script,header=report-to
avtovzglyad.ru,dni.ru,baby.ru,selflib.me#$#abort-on-property-read Ya
kakprosto.ru#$#abort-current-inline-script document.createElement String.fromCharCode; abort-on-property-write window.yaProxy; abort-on-property-write Object.defineProperty
||liveinternet.ru^$xmlhttprequest,header=Uniformat-Product-Type
otzovik.com#$#abort-on-property-read Object.prototype.RtbBlockCreateWrapper
gismeteo.by,gismeteo.kz,gismeteo.md,gismeteo.ru#$#abort-on-property-read Object.prototype.showRTB
yandex.by,yandex.fr,yandex.kz,~telephony.yandex.ru,yandex.ru,yandex.ua,yandex.uz##[class*="mg-grid__"]>div[class*=_status_success]
razlozhi.ru,24smi.org,auto.ru,gorodrabot.ru,kinopoisk.ru#$#abort-on-property-read Element.prototype.attachShadow; abort-on-iframe-property-read Element.prototype.attachShadow
24smi.org#$#abort-on-property-write document.cookie; cookie-remover /^/
gorodrabot.ru#$#abort-on-property-read Object.prototype.storeBlockSettings
||doramatv.live^$header=x-yandex-req-id
drive2.ru#$#abort-on-property-read Object.prototype.loadBanner

! #CV-659
ringside24.com,gagadget.com#$#abort-on-property-read localStorage
newsyou.info#$#abort-on-property-read localStorage
24tv.ua##.b_reklama
azov-sea.info##[valign="middle"]
azov-sea.info##td[width="330"]
boxnews.com.ua##.bn-banner
boxnews.com.ua##[style="padding-bottom: 20px;"]
inforesist.org##aside[id="text-3"]
inforesist.org##aside[id="text-44"]
inforesist.org##aside[id="text-39"]
inforesist.org##aside[id="execphp-30"]
inforesist.org##aside[id="execphp-24"]
etextlib.ru##.partners
etextlib.ru##.head_bann
kinohome.net##a[data-i]
indoxx1.show##img[src^="blob:https://indoxx1.show/"]
/img.akubebas.com^$image,script,domain=indoxx1.show

! #CV-250
gidonline.eu,torrent-soft.net#$#abort-on-property-read atob

! MISC
sm.news#$#abort-current-inline-script decodeURIComponent blocked
7ogorod.ru,autonevod.ru,shtrafsud.ru,tophallclub.ru,zazloo.ru#$#abort-on-property-read fpm_attr
hitfm.ua,kissfm.ua,radiorelax.ua,radioroks.ua,rusradio.ua#$#abort-on-property-write prerole_length
www.goha.ru,3dnews.ru,avtovzglyad.ru,baby.ru,dni.ru,kufar.by,ura.news#$#abort-on-property-read Ya
myjane.ru,omskpress.ru,astrakhan.ru,tambovnet.org#$#abort-on-property-read kav_cn
krolik.biz#$#abort-current-inline-script document.createElement
sinoptik.ua#$#abort-on-property-read SIN.globals.ADBLOCK_DETECTED
agroportal.ua#$#abort-on-property-read abn
kaztorka.org#$#abort-current-inline-script document.createElement Math.random
naruto-base.su#$#abort-on-property-read njranu
animekun.ru,doramakun.ru,nnmclub.to#$#abort-current-inline-script document.createElement Math.random; abort-current-inline-script setInterval Math.random
vestivrn.ru#$#abort-on-property-read video.preroll
rutor.in#$#abort-current-inline-script document.createElement ExternalChromePop
dota2.ru#$#abort-on-property-read helpUsImproveSite
fastpic.org,~new.fastpic.org#$#abort-current-inline-script addEventListener DOMContentLoaded; abort-current-inline-script document.createElement Math.random; prevent-listener /click|load/ popMagic; prevent-listener getexoloader; abort-on-property-read ExoSupport
softportal.com#$#abort-on-property-read popupShowInterval; abort-on-property-read Object.prototype.str_replace
www.ixbt.com#$#abort-on-property-read adv_obj; abort-current-inline-script document.getElementById branding
city.ogo.ua#$#abort-on-property-read myatu_bgm
erofishki.cc,fishki.net#$#abort-on-property-read fishki.adv
botanichka.ru,itech.co.ua,agronom.com.ua,mediasat.info,fainaidea.com,1informer.com,dyvys.info#$#abort-on-property-read td_ad_background_click_target
gdz-putina.fun,gdz.ninja,gdz.ru,gdzotputina.club,gdzputina.net,gdzputina.ru,megaresheba.com,megaresheba.ru,resheba.me,spishi.fun,zoobrilka.net#$#abort-on-property-write app.book.external
||sports.ru^$script,header=x-robots-tag
||www.e1.ru^$script,header=x-yandex-req-id
volynpost.com#$#abort-on-property-read trek_shownow
ati.su#$#abort-on-property-read Object.prototype._bannerOrderAttr
fssp.gov.ru#$#abort-on-property-read tingle
allboxing.ru#$#abort-current-inline-script document.querySelector open
kurs.com.ua#$#abort-current-inline-script document.documentElement.clientWidth
gamebomb.ru#$#abort-current-inline-script jQuery /Position/
sound-park.rocks,sound-park.world,soundpark.rocks#$#abort-current-inline-script $ open
shaiba.kz#$#abort-current-inline-script $ 1xbet
liveforums.ru#$#abort-on-property-read tnAdditionalParams
musify.club#$#abort-current-inline-script $ get_ya_browser
fenglish.site,mp3spy.cc#$#abort-current-inline-script JSON.parse atob
times.zt.ua,f1comp.ru,tagaev.com#$#abort-on-property-read PUM.getPopup
l2top.ru#$#abort-current-inline-script $ cleanAndEval
rsload.net#$#abort-current-inline-script window.addEventListener load
musify.club#$#abort-on-property-read get_ya_browser
kakoysegodnyaprazdnik.ru,xn--80aaiebcrjcibi8adgdtsm9z.xn--p1ai#$#abort-on-property-read google_jobrunner
cq.ru#$#prevent-listener DOMContentLoaded 'banners'
||cq.ru^$xmlhttprequest,header=cq-cache-bypassed
shanson320.ru#$#abort-current-inline-script document.createElement atob
coderlessons.com,facenews.ua,fixx.one,its-kids.ru,molitvy.guru,nizhny.ru,pro100hobbi.ru,publy.ru,samelectric.ru,svadba.expert,tehnobzor.ru,vibir.ru,100popugaev.ru#$#abort-on-property-read XMLHttpRequest
lrepacks.net#$#abort-current-inline-script document.createElement 'delete window'
igroutka.ru#$#override-property-read Object.prototype.afg true
sibnet.ru#$#prevent-listener DOMContentLoaded 'smartweek'
ivanovonews.ru#$#json-prune banner.ytcode
porngames.su,rintor.info,rintor.net#$#abort-on-property-read TotemToolsObject
hardwareluxx.ru#$#abort-current-inline-script jQuery backgroundImage
4pda.to#$#hide-if-matches-xpath './/a[@target="_blank"]/img[@alt="" and @title=""]/parent::a/ancestor::*[@id and @class][1]'; hide-if-matches-xpath './/a[@href][@title][@target="_blank"]/img[@itemprop]/parent::a[@href]'; hide-if-matches-xpath './/a[@href][@target="_blank"]/img[@title and @border]/ancestor::*[2]'; hide-if-matches-xpath './/*[@id and @class]/article[@class="post" and @itemscope]/ancestor-or-self::*[1]';
igromania.ru,kanobu.ru#$#json-prune results.fixed
motorpage.ru#$#json-prune '0.BannerId 1.BannerId 2.BannerId 3.BannerId 4.BannerId 5.BannerId 6.BannerId'
nsportal.ru#$#simulate-mouse-event 'a#downloadLink[href^="https://nsportal.ru/sites/"]'; simulate-mouse-event 'a#downloadURL[href^="https://nsportal.ru/sites/"]'
okminigames.mail.ru#$#override-property-read appDriver.hasVideoAd false
bombardir.ru#$#abort-on-property-read alert
www.e1.ru#$#abort-on-property-read Object.prototype.brandingBlock; abort-on-property-read Ya; cookie-remover jtnews_adblock;
biqle.org,biqle.ru#$#prevent-listener /click|mousedown/ 'popunder'
relax-fm.ru,rg.ru#$#abort-on-property-read AdFox_getCodeScript
ontivi.net#$#abort-current-inline-script document.createElement Math.random
bstudy.net#$#abort-on-property-read _0xddc4
studbooks.net#$#abort-on-property-read _0x8336
studme.org#$#abort-on-property-read _0x5443
studref.com#$#abort-on-property-read _0x308a
studwood.net#$#abort-on-property-read _0x8cc0
vuzlit.com#$#abort-on-property-read _0xc091
xstud.org#$#abort-on-property-read _0xd07e
ozlib.com#$#abort-on-property-read _0xb7d3
romakatya.ru#$#prevent-listener load AdBlock
testserver.pro#$#abort-current-inline-script window.$ adsbygoogle
ashdi.vip,tortuga.wtf#$#json-prune vast
||pikabu.ru/ajax/?key=$xmlhttprequest,header=ssr
vprognoze.ru#$#abort-on-property-read bannersBillboard
vk.com,vk.ru#$#abort-on-property-read vk__adsLight.adsShowed
transkarpatia.net#$#abort-current-inline-script document.createElement redtram
terrikon.com#$#abort-on-property-read bdy
sports.ru#$#abort-on-property-read Sports.pageData.browserTime
sdamgia.ru#$#abort-on-property-read Object.prototype.getRenderToElement
||vgtimes.ru^$xmlhttprequest,header=x-yandex-req-id
liveball.cc,liveball.uno#$#abort-on-property-read ABNS
zab.ru#$#abort-on-property-read document.body.style
volley.ru#$#override-property-read Object.prototype.preroll null

! Prevent auto reload
24boxing.com.ua,bilshe.com,businessua.com,f1analytic.com,f1ua.org.ua,football-ukraine.com,footballgazeta.com,footballtransfer.com.ua,glianec.com,nashamama.com,sportanalytic.com,stravy.net,zdorovia.com.ua#$#abort-current-inline-script setInterval reload

! Popup/popunder/clickunder
freehat.cc,hlamer.ru,lostpix.com,oxax.tv,cunofilms.ru,potokcdn.com,prostoporno.help,tivix.co,tt-cup.com,uploadimagex.com,vmusi.ru,volynpost.com,wowskill.ru,xittv.net,zerno-ua.com#$#abort-on-property-read open
3dn.ru,domahatv.com,piratam.net,piratca.net,sexitorrent.com,sextor.org,xtorrent.net,xxxrip.net,torrent-pirat.com,porn720.biz,pornopuk.com,hentai-share.one,huyamba.tv,xxxtor.com#$#abort-current-inline-script document.querySelectorAll popMagic
hdkinoteatr.com#$#abort-on-property-read cuec
fapreactor.com,pornreactor.cc#$#abort-current-inline-script Math decodeURIComponent
budport.com.ua#$#abort-current-inline-script $ append
aces.gg#$#abort-current-inline-script $ window.open
penzainform.ru#$#abort-current-inline-script $ divWrapper
drlink.online#$#abort-current-inline-script Math.random toString
freehat.cc#$#abort-on-property-read advFirstClickOpenNewTab
anifap.com,anitokyo.org,anitokyo.tv,hcdn.online,kinofilm.co,anipoisk.org#$#abort-on-property-read ClickUndercookie
cosplay-porn.one#$#abort-on-property-read biads
mag.relax.by#$#abort-on-property-read Object.prototype.branding_url
conversion.im#$#abort-current-inline-script $ mainContainer
freescreens.ru,imgbase.ru,imgclick.ru,payforpic.ru,picclick.ru,picforall.ru#$#abort-current-inline-script JSON.parse
tapochek.net#$#abort-on-property-read ads_script
rutor.org#$#abort-on-property-read Light.Popup
bez-smenki.ru#$#abort-current-inline-script $ showPopupextra
partnerkin.com#$#override-property-read Object.prototype.autoPopups undefined
comedy-radio.ru,radioromantika.ru,veseloeradio.ru#$#abort-on-property-read yaContextCb
tornado.3dn.ru#$#abort-on-property-read wrapper.addEventListener

! Video ads
yastatic.net#$#abort-on-property-read Object.prototype.vast

! #CV-547
auto.mail.ru,hi-tech.mail.ru,cloud.mail.ru,horo.mail.ru,pogoda.mail.ru,tv.mail.ru#$#abort-on-property-write _mimic
horo.mail.ru,pogoda.mail.ru,tv.mail.ru#$#abort-on-property-read MrgContext
minigames.mail.ru#$#abort-on-property-read MG_CONFIG.adv.fit_mimic
mmminigames.mail.ru#$#abort-current-inline-script addEventListener advRefreshInterval

! #CV-713
3dn.ru,a-point.info,addfiles.ru,akkordam.ru,all-for-kompa.ru,asia-tv.su,at.ua,autosimgames.ru,clan.su,db-energo.ru,devdrivers.ru,do.am,elegos.ru,elektronika56.ru,elektrosat.ru,fon-ki.com,for-gsm.ru,free-dream.ru,ftechedu.ru,fukushima-news.ru,gals.md,gloria-cedric.ru,goldformat.ru,greenflash.su,igrul-ka.ru,krasnickij.ru,krolmen.ru,megaclips.net,mow-portal.ru,moy.su,my1.ru,narod.ru,newgames.com.ua,novospasskoe-city.ru,omsimclub.ru,online-supernatural.ru,only-paper.ru,others.name,pidru4nik.com,pkrc.ru,play-force.ru,pohoronnoe-byuro.com,pokatushki-pmr.ru,pro-zakupki.ru,project-ss.ru,psxworld.ru,radiodom.org,rocketdockfree.ru,sdr-deluxe.com,soft-game.net,stalker-gsc.ru,stalker-zone.info,stalkermods.ru,svadbatomsk.ru,techmusic.ru,tes-game.ru,torfiles.ru,ucoz.club,ucoz.com,ucoz.net,ucoz.org,ucoz.ru,ucoz.ua,usite.pro,vodopads.ru,vsthouse.ru,xakevsoft.ru,xn--80aeshkkbdj.xn--p1ai,yaminecraft.ru,zona-stalkera.ru#$#abort-on-property-read Object.prototype.clunduse; abort-on-property-read k_init; abort-on-property-read Object.prototype.u_vastplayer
kinogo.eu,gidonline.eu#$#abort-on-property-read target_script

! #CV-742
vesti.ua#$#abort-current-inline-script document.createElement atob

! #CV-685
vsetut.su,animedia.tv#$#abort-on-property-read zfgformats

! CV-715
informator.ua,kriminal.tv,4studio.com.ua,112ua.tv,newsoneua.tv,gorodkiev.com.ua,volynpost.com,uanews.org.ua,dialogs.org.ua,technoportal.com.ua,pingvin.pro,fakty.ua,cikavosti.com,versii.if.ua#$#abort-on-property-read weekCallbacks

! #CV-716
kop.net.ua,1news.com.ua,365news.biz,4mama.ua,4studio.com.ua,7days-ua.com,agroter.com.ua,alter-science.info,apnews.com.ua,argumentiru.com,asiaplustj.info,autotema.org.ua,autotheme.info,avtodream.org,beauty.ua,begemot-media.com,begemot.media,chas.cv.ua,cheline.com.ua,cikavosti.com,ck.ua,cn.ua,comments.ua,cvnews.cv.ua,day.kyiv.ua,depo.ua,dnews.dn.ua,dv-gazeta.info,dyvys.info,economistua.com,edinstvennaya.ua,ekovolga.com,expert.in.ua,fedpress.ru,firtka.if.ua,forpost.media,fraza.com,gazeta1.com,glavnoe.ua,glavnoe24.ru,glavpost.ua,golosinfo.com.ua,gorodkiev.com.ua,grad.ua,greenpost.ua,ifnews.org.ua,inforpost.com,inkorr.com,itechua.com,iz.com.ua,kh.ua,khersonline.net,kolizhanka.com.ua,kr.ua,krymr.com,kurskcity.ru,liga.net,lvnews.org.ua,mega-music.pro,mi100.info,mind.ua,moirebenok.ua,mycompplus.ru,nakanune.ru,promin.cv.ua,mistosumy.com,nashbryansk.ru,newsua.one,newsoneua.tv,news24today.info,ngp-ua.info,nnews.com.ua,novavlada.info,novynarnia.com,np.pl.ua,odessa-life.od.ua,ogo.ua,oukr.info,panoptikon.org,pg11.ru,pik.net.ua,pingvin.pro,planetanovosti.com,pl.com.ua,podpricelom.com.ua,politnavigator.net,poltava365.com,portal.lviv.ua,pravdatutnews.com,prm.ua,procherk.info,profootball.ua,radiosvoboda.org,ratel.kz,real-vin.com,reporter.ua,risu.ua,rivne.media,rivnenews.com.ua,rusjev.com,russianshowbiz.info,rv.ua,rvnews.rv.ua,showdream.org,sport-kr.com.ua,strana.news,strana.today,sud.ua,te.ua,tenews.org.ua,telekritika.ua,theageoffootball.com,treebuna.info,tverigrad.ru,tverisport.ru,tvoymalysh.com.ua,uainfo.org,uanews.org.ua,uatv.ua,ukranews.com,ukrrain.com,unn.com.ua,vchaspik.ua,versii.if.ua,vgolos.com.ua,viva.ua,vlast.kzvnn24.ru,volnorez.com.ua,volyninfa.com.ua,volyninfo.com,volynpost.com,volynua.com,vsviti.com.ua,westnews.info,womo.ua,wona.com.ua,wworld.com.ua,zbirna.com,zp.ua#$#abort-current-inline-script document.createElement /ru-n4p|ua-n4p|загрузка.../
marieclaire.ua#$#abort-current-inline-script redram /загрузка.../
karpatnews.in.ua,kg-portal.ru#$#abort-current-inline-script document.createElement Math.random
vsviti.com.ua#$#abort-on-property-read tdAnimationStack.init

! #CV-667
empireg.ru#$#abort-current-inline-script Math zfgloaded

! #CV-697
agroreview.com#$#abort-current-inline-script encodeURIComponent rcBuf; abort-current-inline-script Object.defineProperty rcBuf; abort-on-property-read advanced_ads_ready;
